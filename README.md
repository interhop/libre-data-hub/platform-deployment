[[_TOC_]]

# Data Platform
The **ETL** tool is Airflow. The **datawarehouse** is greenplum. The
**datascience environment** is jupyterhub with both python & R. The
**dashboard** tools are dash & shiny.

PostgreSQL includes several extensions: `timescaledb` for **signal
 processing**, `postgis` for **spacial and geographic**.

All the accesses on the database are logged. All the notebooks call
are also logged.


``` text
               +----------+
               |          |
               | Jupyter  |
     +---------+ Notebook +-----------+-----------------+
     |         |          |           |                 |
     |         |          |           |                 |
     |         +----^-----+           |                 |
     |              |                 |                 |
     |              |                 |                 |
+----+---+     +----+-----+     +-----+---+             |
|        |     |          |     |         |             |
| Shiny  |     | Greenplum|     | Airflow |             |
|        |     | Data     |     |         +-------------+
|        <-----+ Platform +----->         |             |
|        |     |          |     |         |             |
|        |     |          |     |         |             |
+----+---+     +----------+     +-----^---+             |
     |                                |                 |
     |                                |                 |
     |                                |                 |
     |                                |                 |
     |         +---------+      +-----+---+       +-----v-----+
     |         |         |      |         |       |           |
     |         | Gitlab  |      | Postgres|       | Grafana   |
     +--------->         <------+ Database+<------+ Prometheus|
               |         |      |         |       |           |
               |         |      |         |       |           |
               +---------+      +---------+       +-----------+

```

# Prerequisite
1. install virtualbox
1. install vagrant
1. install ansible


# ansible

``` bash
# setup a virtualenv
virtualenv venv
source venv/bin/activate
# install ansible
pip install ansible==2.9.16
ansible-galaxy collection install community.docker
```

# Test installation
The below command will download and install a debian 10 virtual
machine and deploy it on ip: **192.168.33.10**. You can modify the
ansible scripts and “provision” by mean re-run them. You can also
destroy to test the deploiement from scratch.

``` bash
# first time
vagrant up

# provision
vagrant provision

# destroy 
vagrant destroy
```

# Modules
## Postgresql
[source code](https://github.com/ANXS/postgresql)
Note:
- encrypted password with https://stackoverflow.com/a/50475676/3865083

## Greenplum
[source code](https://gpdb.docs.pivotal.io/6-0/install_guide/ansible-example.html)

## Airflow
[source code](https://github.com/tulibraries/ansible-role-airflow)

## Jupyterhub
[littest juphterhub](https://tljh.jupyter.org/en/latest/index.html)
in particular {the installation](https://tljh.jupyter.org/en/latest/topic/customizing-installer.html)

non utilisé:
- [source code](https://gitlab.com/opendatahub/jupyterhub-ansible)
- [may be usefull](https://github.com/jupyterhub/repo2docker)
- [jupyterhub paris saclay](https://gitlab.in2p3.fr/jupyterhub-paris-saclay/jupyterhub-configuration)

## Shiny
[source code](https://github.com/Oefenweb/ansible-shiny-server)

## Dash
## Gitlab
[source code](https://github.com/geerlingguy/ansible-role-gitlab)
## Certbot
